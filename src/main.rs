use image::{GenericImage,GenericImageView, imageops, ImageFormat, ImageBuffer};
use std::{fs, env, path};
//use std::thread;
use crossbeam::thread;


// fn print_type_of<T>(_: &T) {
//     println!("{}", std::any::type_name::<T>())
// }

fn get_crop_dim(crop_dir_path: &str) -> (u32,u32) {

    // has to be initialized
    let mut first_path: path::PathBuf = path::Path::new("plop/").to_path_buf();

    for entry in fs::read_dir(crop_dir_path).expect("") {
        let entry = entry.expect("File could not be read");
        let path = entry.path();
        first_path = path;
        break; //nasty way of taking only the first path
    }
    let img = image::open(first_path).unwrap();
    let (x,y) = img.dimensions();
    (x,y)
}

fn get_final_image_dimensions(crop_dir_path: &str) -> (u32,u32) {

    // list all the elements in the dir containing the crops
    // https://stackoverflow.com/questions/66577339/collect-file-names-into-vecstr
    let mut stringvector: Vec<String> = std::fs::read_dir(crop_dir_path).unwrap()
        .filter_map(|maybe_dir_entry| {
            let dir_entry = maybe_dir_entry.ok()?;
            let path_buf = dir_entry.path();
            let file_name = path_buf.file_name()?;
            let string = file_name.to_str()?; 
            Some(string.to_string())
        })
        .collect();
    stringvector.sort();
    let last = stringvector.last().cloned().unwrap();

    // get dimensions of crop
    let (x,y) = get_crop_dim(crop_dir_path);

    // get coordinates of last crop
    let split = last.split(".");
    let vec = split.collect::<Vec<&str>>();
    let without_ext = vec.into_iter().nth(0).unwrap(); //destroys vec
    let split2 = without_ext.split("_");
    let vec2 = split2.collect::<Vec<&str>>();
    
    let x_str = vec2[0].to_string();
    let y_str = vec2[1].to_string();
    let x_coord = x_str.parse::<u32>().unwrap();
    let y_coord = y_str.parse::<u32>().unwrap();
    
    // get original image dimensions
    let x_img = x_coord + x;
    let y_img = y_coord + y;
    (x_img,y_img)
}


fn main() {

    // parse and check input args
    let args: Vec<String> = env::args().collect();
    let argsourcepath = &args[1];
    let argoutpath = &args[2];


    // check if source file exist
    if !(path::Path::new(argsourcepath).exists()) {
        eprintln!("{} does not exist !",argsourcepath);
        return;
    }

    let (x_img,y_img) = get_final_image_dimensions(argsourcepath);

    // https://github.com/image-rs/image/blob/master/examples/concat/main.rs
    // Create a new ImgBuf with width: imgx and height: imgy
    let mut imgbuf = image::ImageBuffer::new(x_img, y_img);
    
    for entry in fs::read_dir(argsourcepath).expect("") {
        let entry = entry.expect("File could not be read");
        let path = entry.path();
        // avoid move conflict between image and str. parsing
        let pathstr = entry.path(); 

        let img = image::open(path).unwrap();

        // parse name to get coordinates
        let unwr_path = pathstr.into_os_string().into_string().unwrap();
        let split = unwr_path.split(".");
        let vec = split.collect::<Vec<&str>>();
        let without_ext = vec.into_iter().nth(0).unwrap(); //destroys vec
        let split2 = without_ext.split("_");
        let vec2 = split2.collect::<Vec<&str>>();
        
        // x coordinate
        let x_str_temp = vec2[0].to_string();
        let split3 = x_str_temp.split("/");
        let vec3 = split3.collect::<Vec<&str>>(); // "crop/." remains
        let x_str = vec3[1].to_string();
        let x_coord = x_str.parse::<u32>().unwrap();

        // y coordinate
        let y_str = vec2[1].to_string();
        let y_coord = y_str.parse::<u32>().unwrap();

        println!("{} {}", x_coord, y_coord);
        // copy into new buffer
        imgbuf.copy_from(&img, x_coord, y_coord).unwrap();
    }
    imgbuf.save(argoutpath).unwrap();
}
